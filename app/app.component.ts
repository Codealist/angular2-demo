import {Component} from "angular2/src/core/metadata";
import {ItemListComponent} from "./shopping-list.component";

@Component({
    selector: 'my-app',
    template: `
    <my-list></my-list>
    `,
    directives:[ItemListComponent]
})
export class AppComponent {}
